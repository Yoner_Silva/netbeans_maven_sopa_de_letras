package Negocio;

/**
 * Write a description of interface OperacioMatriz here.
 *
 * @author yoner_silva
 * @version (a version number or a date)
 */
public interface OperacioMatriz {

    public int getCantCol(int fila);

    public int filaMasDatos();

    public String izquierdaDerecha();

    public String derechaIzquierda();

    public String arribaAbajo();

    public String abajoArriba();
}
