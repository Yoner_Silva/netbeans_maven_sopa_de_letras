package Negocio;

import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

//Para imprimir pdf
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import javax.swing.JOptionPane;

/**
 * Write a description of class SopaLetra here.
 *
 * @author yoner_silva
 * @version (a version number or a date)
 */
public class SopaDeLetras implements OperacioMatriz {
    // La matriz con las letras de la sopa

    private char sopa[][];
    private String palabra = "";
    private String FileName = "";
    private String name = "";

    //Constructor por defecto.
    public SopaDeLetras() {

    }

    public SopaDeLetras(String FileName, String palabra) {
        this.FileName = FileName;
        this.palabra = palabra.toUpperCase();
        //this.name = name;

    }

    /*public SopaDeLetras(String palabras) throws Exception {
        if (palabras == null || palabras.isEmpty()) {
            throw new Exception("No se puede crear la sopa");
        }

        String palabras2[] = palabras.split(",");
        //Crear la cantidad de filas:
        this.sopa = new char[palabras2.length][];
        //recorrer cada elemento de palabras2 y pasarlo a su correspondiente fila en sopa:
        int i = 0;
        for (String palabraX : palabras2) {
            this.sopa[i] = new char[palabraX.length()];
            pasar(palabraX, this.sopa[i]);
            i++;
        }

    }*/
    private void pasar(String x, char vector[]) {
        for (int j = 0; j < x.length(); j++) {
            vector[j] = x.charAt(j);
        }
    }

    public String toString() {
        String msg = "";
        for (int i = 0; i < this.sopa.length; i++) {
            for (int j = 0; j < this.sopa[i].length; j++) {
                msg += this.sopa[i][j] + " " + "\t";
            }
            msg += "\n";
        }
        return msg;
    }

    public String toString2() {
        String msg = "";
        for (char vector[] : this.sopa) {
            for (char dato : vector) {
                msg += dato + "" + "\t";
            }
            msg += "\n";
        }
        return msg;
    }

    /**
     * Retorna la letra que más se repite
     */
    public char get_MasSeRepite() {
        char letra = ' ';
        int cont = 0;
        int letraRepetida = 0;
        for (int i = 0; i < sopa.length; i++) {
            for (int j = 0; j < sopa[i].length; j++) {
                char letra2 = sopa[i][j];
                for (int k = 0; k < sopa.length; k++) {
                    for (int l = 0; l < sopa[k].length; l++) {
                        if (letra2 == sopa[k][l]) {
                            cont++;
                        }
                    }
                }
                if (cont > letraRepetida) {
                    letraRepetida = cont;
                    letra = letra2;
                }
                cont = 0;
            }
        }
        return letra;
    }

    public boolean esRectangular() {
        for (int i = 0; i < sopa.length; i++) {
            if (!(sopa.length != sopa[i].length) || !(sopa[i].length != sopa.length)) {
                return false;
            }
        }
        return true;
    }


    public boolean esDispersa() {
        for(int i = 0,cont = 1; i < this.sopa.length;i++,cont++){
            if(cont == sopa.length){cont--;}
            if(sopa[i].length != sopa[cont].length){
                return true;
            }
        }
        return false;
    }

    public boolean esCuadrada() {
        for (int i = 0; i < sopa.length; i++) {
            if (sopa[i].length != sopa.length) {
                return false;
            }
        }
        return true;
    }

    public char[] getDiagonalInferior() throws Exception {
        //si y solo si es cuadrada , si no, lanza excepcion
        if (esCuadrada()) {
            char[] arreglo = new char[sopa.length];
            for (int i = 0; i < sopa.length; i++) {
                for (int j = 0; j < sopa[i].length; j++) {
                    if (i + j == sopa.length - 1) {
                        arreglo[i] = sopa[i][j];
                    }
                }
            }
            return arreglo;
        } else {
            throw new Exception("No es cuadrada.");
        }
    }


    //Start GetterSetterExtension Code
    /**
     * Getter method sopa
     */
    public char[][] getSopa() {
        return this.sopa;
    }//end method getSopa

    /**
     * Setter method sopa
     */
    public void setSopa(char[][] sopa) {
        this.sopa = sopa;
    }

    public String getPalabra() {
        return this.palabra;
    }

    public void setName(String name) {
        this.name = name;
    }//end method setSopa
    //End GetterSetterExtension Code
    //!

    public int getCantCol(int fila) {
        if (sopa == null || fila >= sopa.length || fila < 0) {
            return -1;
        } else {
            return sopa[fila].length;
        }
    }

    public int filaMasDatos() {
        int cont = 0;
        int valor = 0;
        if (sopa == null) {
            return -1;
        } else {
            for (int i = 0; i < sopa.length; i++) {
                for (int j = 1; j < sopa.length; j++) {
                    cont = sopa[i].length;

                    if (cont < sopa[j].length) {
                        valor = j;
                    }
                }
                return valor;
            }

        }
        return -1;
    }

    //Lee el archivo excel y lo almacena en una matriz(sopa). El archivo solo debe ser de tipo(xls).
    public void leerExcel(String FileName) throws Exception {

        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(FileName));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum() + 1;
        int cantCol = 0;

        this.sopa = new char[canFilas][];

        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            cantCol = filas.getLastCellNum();

            this.sopa[i] = new char[cantCol];

            for (int j = 0; j < cantCol; j++) {
                //Obtiene la celda y su valor respectivo
                //double r=filas.getCell(i).getNumericCellValue();
                this.sopa[i][j] = filas.getCell(j).getStringCellValue().toUpperCase().charAt(0);
            }
        }
        if (esDispersa()) {
            this.sopa = null;
            JOptionPane.showMessageDialog(null, "Error, la matriz es dispersa.");

        }
    }

    public String Parrafo() throws Exception {
        try {
            leerExcel(this.FileName);
            String parrafo1 = "";
            parrafo1=("\t" + "Sopa De Letras" + "\n\n");
            for (int i = 0; i < this.sopa.length; i++) {
                for (int j = 0; j < this.sopa[i].length; j++) {
                    parrafo1+=(sopa[i][j] + "\t");
                }
                parrafo1+=("\n");
            }

            return parrafo1;
        } catch (Exception e) {
            System.err.println("Error al rellenar parrafo.");
        }
        return " ";
    }

    //Imprime la matriz, y los metodos de mas abajo en un archivo pdf creado por este método.
    public void pdf() throws Exception {

        try {
            Document pdf = new Document();
            Paragraph parrafoCopia = new Paragraph();
            PdfWriter.getInstance(pdf, new FileOutputStream(this.name + ".pdf"));
            pdf.open();
            parrafoCopia.add(Parrafo());
            parrafoCopia.add(izquierdaDerecha());
            parrafoCopia.add(derechaIzquierda());
            parrafoCopia.add(arribaAbajo());
            parrafoCopia.add(abajoArriba());
            pdf.add(parrafoCopia);
            pdf.close();
            if (Parrafo().equals(" ")) {
                File f = new File(this.name+".pdf");
                f.delete();               
            } else {
                Desktop.getDesktop().open(new File(this.name + ".pdf"));
            }

        } catch (DocumentException | IOException e) {

            JOptionPane.showMessageDialog(null, "Error al crear PDF");
            System.err.println(e.getMessage());

        }

    }

    //Estos metodos son para encontrar la palabra buscada en la sopa de letras.
    //Retorna un String con el numeor de incidencias enocntradas y su ubicacion.
    public String izquierdaDerecha() {
        try {
            leerExcel(this.FileName);
            String palabra2 = "";
            byte k = 0, num = 0;
            String cadena = "";
            for (int i = 0; i < sopa.length; i++) {
                palabra2 = "";
                k = 0;
                for (int j = 0; j < sopa[i].length; j++) {
                    if (sopa[i][j] == this.palabra.charAt(k)) {
                        k++;
                        if (k == this.palabra.length()) {
                            k = 0;
                        }
                        palabra2 += sopa[i][j];
                        if (this.palabra.equals(palabra2)) {
                            num++;
                            palabra2 = "";
                            k = 0;
                            cadena += "En la fila: " + i + " - Entre Columnas: " + Math.abs(j - palabra.length() + 1) + " y " + j + "\n";
                        }
                    } else {
                        palabra2 = "";
                        k = 0;
                    }
                }
            }
            return "\n"+"La palabra " + this.palabra + " se encontro " + num + " veces de izquierda a derecha. " + "\n" + cadena;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error al buscar de izquierda a derecha.");
            System.err.println(ex.getMessage());
        }
        return "";
    }

    public String derechaIzquierda() {
        try {
            leerExcel(this.FileName);
            String palabra2 = "";
            byte k = 0, num = 0;
            String cadena = "";
            for (int i = 0; i < sopa.length; i++) {
                palabra2 = "";
                k = 0;
                for (int j = sopa[i].length-1; j > -1; j--) {
                    if (sopa[i][j] == this.palabra.charAt(k)) {
                        k++;
                        if (k == this.palabra.length()) {
                            k = 0;
                        }
                        palabra2 += sopa[i][j];
                        if (this.palabra.equals(palabra2)) {
                            num++;
                            palabra2 = "";
                            k = 0;
                            cadena += "Fila: " + i + " - Entre Columna: " + Math.abs((sopa[i].length - 1) - j) + " y " + Math.abs((sopa[i].length - j) - this.palabra.length()) + "\n";
                        }
                    } else {
                        palabra2 = "";
                        k = 0;
                    }
                }
            }
            return "La palabra " + this.palabra + " se encontro " + num + " veces de derecha a izquierda." + "\n" + cadena;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error al buscar de derecha a izquierda.");
            System.err.println(ex.getMessage());
        }
        return "";
    }

    public String arribaAbajo() {
        try {
            leerExcel(this.FileName);
            String palabra2 = "";
            byte k = 0, num = 0;
            String cadena = "";
            for (int i = 0; i < sopa[i].length; i++) {
                palabra2 = "";
                k = 0;
                for (int j = 0; j < sopa.length; j++) {
                    if (sopa[j][i] == this.palabra.charAt(k)) {
                        k++;
                        if (k == this.palabra.length()) {
                            k = 0;
                        }
                        palabra2 += sopa[j][i];
                        if (this.palabra.equals(palabra2)) {
                            num++;
                            palabra2 = "";
                            k = 0;
                            cadena += "Columna: " + i + " - Entre Filas: " + j + " y " + Math.abs(j - this.palabra.length() + 1) + "\n";
                        }
                    } else {
                        palabra2 = "";
                        k = 0;
                    }
                }
            }
            return "La palabra " + this.palabra + " se encontro " + num + " veces de arriba a abajo." + "\n" + cadena;

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error al buscar de arriba a abajo.");
            System.err.println(ex.getMessage());
        }
        return "";
    }

    public String abajoArriba() {
        try {
            leerExcel(this.FileName);
            String palabra2 = "";
            byte k = 0, num = 0;
            String cadena = "";
            for (int i = 0; i < sopa[i].length; i++) {
                palabra2 = "";
                k = 0;
                for (int j = sopa.length-1; j > -1; j--) {
                    if (sopa[j][i] != this.palabra.charAt(k)) {
                        k = 0;
                        palabra2 = "";
                    }
                    if (sopa[j][i] == this.palabra.charAt(k)) {
                        k++;
                        if (k == this.palabra.length()) {
                            k = 0;
                        }
                        palabra2 += sopa[j][i];
                        if (this.palabra.equals(palabra2)) {
                            num++;
                            palabra2 = "";
                            k = 0;
                            cadena += "Columna: " + i + " - Entre Filas: " + Math.abs((sopa.length - 1) - j) + " y " + Math.abs(((sopa.length - 1) - j) - this.palabra.length() + 1) + "\n";
                        }
                    } else {
                        palabra2 = "";
                        k = 0;
                    }
                }
            }
            return "La palabra " + this.palabra + " se encontro " + num + " veces de abajo a arriba." + "\n" + cadena;

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error a buscar de abajo a arriba.");
            System.err.println(ex.getMessage());
        }
        return "";
    }
}
